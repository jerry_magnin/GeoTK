from tkinter import Tk
from tkinter import ttk
import tkinter as tk
from computations import *

class UI(tk.Frame):
    def __init__(self, window, **kwargs):
        tk.Frame.__init__(self, window, **kwargs)
        window.title('RockMassRating')
        self.pack(fill=tk.BOTH)

        self.assertions = {
            'Str': 0,
            'RQD': 0,
            'DS': 0,
            'DL': 0,
            'Sep': 0,
            'RG': '',
            'In': '',
            'W': '',
            'GW': ''}
        self.ratings = {
            'Str': 0,
            'RQD': 0,
            'DS': 0,
            'DL': 0,
            'Sep': 0,
            'RG': 0,
            'In': 0,
            'W': 0,
            'GW': 0}

        row = 0

        ###########################
        # Strength of intact rock #
        ###########################
        tk.Label(self, text="Strength of intact rock").grid(row=row, columnspan=4)
        row += 1

        tk.Label(self, text="Strength test method").grid(row=row, column=0)
        testMethods = ('Point-load SI', 'Uniaxial comp SI')
        self.methodVar = tk.StringVar(self)
        self.methodVar.trace('w', self.updateStrength)
        self.strengthMethod = ttk.Combobox(self,
            textvariable=self.methodVar,
            state='readonly',
            values=testMethods).grid(row=row, column=1)
        self.methodVar.set(testMethods[0])
        tk.Label(self, text="Strength value (MPa)").grid(row=row, column=2)
        self.strengthValVar = tk.StringVar(self)
        self.strengthValVar.trace('w', self.updateStrength)
        self.strengthVal = tk.Spinbox(self,
            textvariable=self.strengthValVar).grid(row=row, column=3)

        row += 1

        tk.Label(self, text="Strength rating").grid(row=row, column=0)
        self.strengthRatingVar = tk.StringVar(self)
        self.strengthRating = tk.Entry(self,
            textvariable=self.strengthRatingVar,
            state='readonly').grid(row=row, column=1)



        row = self.addSeparator(row)
        ######################
        # Rock Drill Quality #
        ######################
        tk.Label(self, text="Rock Drill Quality").grid(row=row, columnspan=4)
        row += 1
        tk.Label(self, text="Total length of elts > 10cm (cm)").grid(row=row, column=0)
        self.lenElts = tk.StringVar(self)
        #self.lenElts.trace_add('write', self.updateRQD)
        self.lenElts.trace('w', self.updateRQD)
        self.len10 = tk.Spinbox(self, textvariable=self.lenElts).grid(row=row, column=1)
        tk.Label(self, text="Drill length (cm)").grid(row=row, column=2)
        self.lenDrillVar = tk.StringVar(self)
        #self.lenDrillVar.trace_add('write', self.updateRQD)
        self.lenDrillVar.trace('w', self.updateRQD)
        self.lenDrill = tk.Spinbox(self, textvariable=self.lenDrillVar).grid(row=row, column=3)

        row += 1

        tk.Label(self, text="RQD value").grid(row=row, column=0)
        self.rqdVal = tk.StringVar(self)
        self.rqdValField = tk.Entry(self, textvariable=self.rqdVal, state='readonly').grid(row=row, column=1)
        tk.Label(self, text="RQD rating").grid(row=row, column=2)
        self.rqdRating = tk.StringVar(self)
        self.rqdRatingField = tk.Entry(self, textvariable=self.rqdRating, state='readonly').grid(row=row, column=3)

        row = self.addSeparator(row)

        ###########################
        # Discontinuities spacing #
        ###########################
        tk.Label(self, text="Discontinuities spacing (m)").grid(row=row, column=0)
        self.sdiscVal = tk.StringVar(self)
        self.sdiscVal.trace('w', self.updateSD)
        self.sdisc = tk.Spinbox(self, textvariable=self.sdiscVal).grid(row=row, column=1)

        tk.Label(self, text="Disc. spacing rating").grid(row=row, column=2)
        self.sdiscRatingVal = tk.StringVar(self)
        self.sdiscRating = tk.Entry(self,
            textvariable=self.sdiscRatingVal,
            state='readonly').grid(row=row, column=3)

        row = self.addSeparator(row)
        ##########################
        # Discontinuities length #
        ##########################
        tk.Label(self, text="Discontinuities length (m)").grid(row=row, column=0)

        self.dlVal = tk.StringVar(self)
        self.dlVal.trace('w', self.updateDL)
        self.dlen = tk.Spinbox(self,
            textvariable=self.dlVal).grid(row=row, column=1)

        tk.Label(self, text="Disc. length rating").grid(row=row, column=2)
        self.dlRatingVal = tk.StringVar(self)
        self.dlRating = tk.Entry(self,
            textvariable=self.dlRatingVal,
            state='readonly').grid(row=row, column=3)

        row = self.addSeparator(row)
        ##################
        # Aperture value #
        ##################
        tk.Label(self, text="Aperture value (cm)").grid(row=row, column=0)

        self.avVal = tk.StringVar(self)
        self.avVal.trace('w', self.updateAV)
        self.apertureVal = tk.Spinbox(self,
            textvariable=self.avVal).grid(row=row, column=1)

        tk.Label(self, text="Aperture rating").grid(row=row, column=2)
        self.avRatingVal = tk.StringVar(self)
        self.avRating = tk.Entry(self,
            textvariable=self.avRatingVal,
            state='readonly').grid(row=row, column=3)

        row = self.addSeparator(row)
        #############
        # Roughness #
        #############
        tk.Label(self, text="Roughness index").grid(row=row, column=0)
        self.roughVal = tk.StringVar(self)
        self.roughVal.trace('w', self.updateRoughness)

        roughnesses = ('Very Rough', 'Rough', 'Slightly Rough', 'Smooth', 'Slickensided')
        self.roughness = ttk.Combobox(self,
            textvariable=self.roughVal,
            state='readonly',
            values=roughnesses).grid(row=row, column=1)

        tk.Label(self, text="Roughness rating").grid(row=row, column=2)
        self.roughRatingVal = tk.StringVar(self)
        self.roughRating = tk.Entry(self,
            textvariable=self.roughRatingVal,
            state='readonly').grid(row=row, column=3)
        
        row = self.addSeparator(row)
        #############
        # Infilling #
        #############
        tk.Label(self, text="Infilling").grid(row=row, column=0)
        self.infillVal = tk.StringVar(self)
        self.infillVal.trace('w', self.updateInfill)

        infills = ('None',
            'Hard filling < 5mm',
            'Hard filling > 5mm',
            'Soft filling < 5mm',
            'Soft filling > 5mm')
        self.infill = ttk.Combobox(self,
            textvariable=self.infillVal,
            state='readonly',
            values=infills).grid(row=row, column=1)

        tk.Label(self, text="Infilling rating").grid(row=row, column=2)
        self.infillRatingVal = tk.StringVar(self)
        self.infillRating = tk.Entry(self,
            textvariable=self.infillRatingVal,
            state='readonly').grid(row=row, column=3)
                
        row = self.addSeparator(row)
        ##############
        # weathering #
        ##############
        tk.Label(self, text="Weathering").grid(row=row, column=0)
        self.weatheringVal = tk.StringVar(self)
        self.weatheringVal.trace('w', self.updateWeathering)

        weatherings = ('Unweathered',
            'Slightly Weathered',
            'Moderately Weathered',
            'Highly Weathered',
            'Decomposed')
        self.weathering = ttk.Combobox(self,
            textvariable=self.weatheringVal,
            state='readonly',
            values=weatherings).grid(row=row, column=1)

        tk.Label(self, text="Weathering rating").grid(row=row, column=2)
        self.weatheringRatingVal = tk.StringVar(self)
        self.weatheringRating = tk.Entry(self,
            textvariable=self.weatheringRatingVal,
            state='readonly').grid(row=row, column=3)

        row = self.addSeparator(row)
        ###############
        # Groundwater #
        ###############
        tk.Label(self, text="Groundwater").grid(row=row, column=0)
        self.gwVal = tk.StringVar(self)
        self.gwVal.trace('w', self.updateGW)

        groundwaterValues = ('Completely Dry',
            'Damp',
            'Wet',
            'Dripping',
            'Flowing')
        self.gwater = ttk.Combobox(self,
            textvariable=self.gwVal,
            state='readonly',
            values=groundwaterValues).grid(row=row, column=1)

        tk.Label(self, text="Groundwater rating").grid(row=row, column=2)
        self.gwRatingVal = tk.StringVar(self)
        self.gwRating = tk.Entry(self,
            textvariable=self.gwRatingVal,
            state='readonly').grid(row=row, column=3)

        row = self.addSeparator(row)
        ###########
        # RESULTS #
        ###########
        tk.Label(self, text="Result").grid(row=row, column=0)
        self.resultVal = tk.StringVar(self)
        self.result = tk.Entry(self,
            textvariable = self.resultVal,
            state='readonly').grid(row=row, column=1)
        tk.Label(self, text="Quality of rock").grid(row=row, column=2)
        self.textResultVal = tk.StringVar(self)
        self.textResult = tk.Entry(self,
            textvariable=self.textResultVal,
            state='readonly').grid(row=row, column=3)

    def addSeparator(self, row):
        row += 1
        ttk.Separator(self, orient=tk.HORIZONTAL).grid(row=row, columnspan=4, sticky='ew')
        row += 1
        return row

    def updateResult(self):
        result = 0
        for rating in self.ratings.values():
            result += rating
        self.resultVal.set(result)
        
        resultAsText = textResult(result)

        self.textResultVal.set(resultAsText)

    def updateStrength(self, *args, **kwargs):
        method= self.methodVar.get()
        try:
            value = int(self.strengthValVar.get())
        except:
            value = 0

        if method == 'Point-load SI':
            result = strengthPointLoad(value)
        elif method == 'Uniaxial comp SI':
            result = strengthUniaxial(value)

        self.strengthRatingVar.set(result)
        self.ratings['Str'] = result
        self.updateResult()


    def updateRQD(self, *args, **kwargs):
        try:
            lenElts = int(self.lenElts.get())
            lenDrill = int(self.lenDrillVar.get())
        except:
            lenElts = 0
            lenDrill = 0
        if lenElts > 0:
            result = lenElts / lenDrill
            self.rqdVal.set(str(result))
            self.rqdRating.set(str(ratingRQD(result)))
            self.ratings['RQD'] = result
            self.updateResult()

    def updateSD(self, *args, **kwargs):
        try:
            discSpacing = float(self.sdiscVal.get())
        except:
            discSpacing = -1
        if discSpacing >= 0:
            result = spacingDisc(discSpacing)
            self.sdiscRatingVal.set(str(result))
            self.ratings['SD'] = result
            self.updateResult()

    def updateDL(self, *args, **kwargs):
        try:
            dLen = int(self.dlVal.get())
        except:
            dLen = -1
        if dLen >= 0:
            result = discLen(dLen)
            self.dlRatingVal.set(str(result))
            self.ratings['DL'] = result
            self.updateResult()

    def updateAV(self, *args, **kwargs):
        try:
            av = float(self.avVal.get())
        except:
            av = -1
        if av >= 0:
            result = separation(av)
            self.avRatingVal.set(str(result))
            self.ratings['Sep'] = result
            self.updateResult()

    def updateRoughness(self, *args, **kwargs):
        roughnesses = {'Very Rough': 'VR',
            'Rough': 'R',
            'Slightly Rough': 'SR',
            'Smooth': 'Sm',
            'Slickensided': 'Sl'}
        rough = roughnesses[self.roughVal.get()]
        result = roughness(rough)
        self.roughRatingVal.set(result)
        self.ratings['RG'] = result
        self.updateResult()

    def updateInfill(self, *args, **kwargs):
        infill = self.infillVal.get()
        infill = infillingRate(infill)
        self.infillRatingVal.set(infill)
        self.ratings['In'] = infill
        self.updateResult()

    def updateWeathering(self, *args, **kwargs):
        weathering = self.weatheringVal.get()
        weathering = weatheringRating(weathering)
        self.weatheringRatingVal.set(weathering)
        self.ratings['W'] = weathering
        self.updateResult()

    def updateGW(self, *args, **kwargs):
        gw = self.gwVal.get()
        gw = groundwaterRating(gw)
        self.gwRatingVal.set(str(gw))
        self.ratings['GW'] = gw
        self.updateResult()


if __name__ == '__main__':
    window = Tk()
    interface = UI(window)
    interface.mainloop()
    interface.destroy()
