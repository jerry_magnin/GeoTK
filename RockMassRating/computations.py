import math

def calcRQD(eltsSup10, drillLength):
    return (eltsSup10/drillLength)

def strengthPointLoad(s):
    if s > 10:
        result = 15
    elif s > 4:
        result = 12
    elif s > 2:
        result = 7
    elif s > 1:
        result = 4
    else:
        result = 0
    return result

def strengthUniaxial(s):
    if s > 250:
        result = 15
    elif s > 100:
        result = 12
    elif s > 50:
        result = 7
    elif s > 25:
        result = 4
    elif s > 5:
        result = 2
    elif s > 1:
        result = 1
    else:
        result = 0
    return result

def ratingRQD(rqd):
    if rqd < 0.25:
        rtn = 3
    elif rqd < 0.5:
        rtn = 8
    elif rqd < 0.75:
        rtn = 13
    elif rqd < 0.90:
        rtn = 17
    else:
        rtn = 20

    return rtn

def spacingDisc(spacing_m):
    if spacing_m < 0.06:
        rating = 5
    elif spacing_m < 0.2:
        rating = 8
    elif spacing_m < 0.6:
        rating = 10
    elif spacing_m < 2:
        rating = 15
    else:
        rating = 20

    return rating

def discLen(length):
    if length < 1:
        rating = 6
    elif length < 3:
        rating = 4
    elif length < 10:
        rating = 2
    elif length < 20:
        rating = 1
    else:
        rating = 0
    return rating

def separation(aperture):
    if aperture == 0:
        rating = 6
    elif aperture < 0.1:
        rating = 5
    elif aperture < 1:
        rating = 4
    elif aperture < 5:
        rating = 1
    else:
        rating = 0
    return rating

def roughness(roughness_idx):
    if roughness_idx == 'VR':
        rating = 6
    elif roughness_idx == 'R':
        rating = 5
    elif roughness_idx == 'SR':
        rating = 3
    elif roughness_idx == 'Sm':
        rating = 1
    else:
        rating = 0
    return rating

def infillingRate(infill):
    if infill == 'None':
        rating = 6
    elif infill == 'Hard filling < 5mm':
        rating = 4
    elif infill == 'Hard filling > 5mm':
        rating = 2
    elif infill == 'Soft filling < 5mm':
        rating = 2
    elif infill == 'Soft filling > 5mm':
        rating = 0
    return rating

def weatheringRating(w):
    if w == 'Unweathered':
        rating = 6
    elif w == 'Slightly Weathered':
        rating = 5
    elif w == 'Moderately Weathered':
        rating = 3
    elif w == 'Highly Weathered':
        rating = 1
    elif w == 'Decomposed':
        rating = 0
    return rating

def groundwaterRating(w):
    if w == 'Completely Dry':
        rating = 15
    elif w == 'Damp':
        rating = 10
    elif w == 'Wet':
        rating = 7
    elif w == 'Dripping':
        rating = 4
    else:
        rating = 0
    return rating
    
def textResult(r):
    if r < 21:
        res = 'Very poor rock'
    elif r <= 40:
        res = 'Poor rock'
    elif r <= 60:
        res = 'Fair rock'
    elif r <= 80:
        res = 'Good rock'
    else:
        res = 'Very good rock'
    return res
